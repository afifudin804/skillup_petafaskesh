var currentFeature;
	
	var dataUrl = "http://localhost:8090/iserver/services/data-Peta_Vaksin/rest/data";  
	  
	function editTable() {  
	    $.each(currentFeature.values_, function(idx, obj) {  
	        if (idx != "smUserID" && idx != "osm_id"  && idx != "building") {  
	            currentFeature.values_[idx] = $("#" + idx + "Input").val();  
	        }  
	    });  
	    console.log(currentFeature);  
	      
	    var addFeatureParams = new SuperMap.EditFeaturesParameters({  
	        features: [currentFeature],  
	        dataSourceName: "Peta_Vaksin_Sleman",  
	        dataSetName: "Hospital",  
	        editType: "update",  
	        returnContent: true  
	    });  
	    var editFeaturesService = new ol.supermap.FeatureService(dataUrl);  
	    editFeaturesService.editFeatures(addFeatureParams, function(serviceResult) {  
	        if (serviceResult.result.succeed) {  
	            alert("Modify successfully!");
				
			layer.getSource().refresh();        
			clearDraw();
	        }else{
                alert("Modify Failed!");  
            }
	    });  
	}
    
	var editSource = new ol.source.Vector();  
	let editLayer = new ol.layer.Vector({  
	    source: editSource  
	});  
	
	var modify, snap;  
	map.addLayer(editLayer);

	function editGeometry() {  
	    editLayer.getSource().clear();  
	    drawLayer.getSource().clear();  
	    resultLayer.getSource().clear();  
	    overlay.setOffset([0, overlay.getOffset()[1]-30]);  
	      
	    modify = new ol.interaction.Modify({  
	        source: editSource  
	    });  
	    map.addInteraction(modify);  
	  
	    snap = new ol.interaction.Snap({  
	        source: editSource  
	    });  
	    map.addInteraction(snap);  
	      
	    editSource.addFeatures([currentFeature]);  
	      
	    modify.on('modifyend', function(evt) {  
	        currentFeature = evt.features.array_[0];  
	    });  
	}
